package br.org.centeleghe;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Centeleghe
 */
public class TelaCadastroController implements Initializable {

    @FXML
    private ToggleGroup tipo;
    @FXML
    private AnchorPane cadastroProfessor;
    @FXML
    private TextField nomeTexto;
    @FXML
    private TextField idadeTexto;
    @FXML
    private RadioButton alunoButton;
    @FXML
    private RadioButton professorButton;
    @FXML
    private RadioButton funcaoAdministrativaButton;
    @FXML
    private TextField enderecoTexto;
    @FXML
    private AnchorPane cadastroAluno;
    @FXML
    private TextField semestreTexto;
    @FXML
    private TextField cursoTexto;
    @FXML
    private AnchorPane CadastroAdm;
    @FXML
    private TextField salarioProfessorTexto;
    @FXML
    private TextField disciplinaTexto;
    @FXML
    private TextField salarioAdmTexto;
    @FXML
    private TextField setorTexto;
    @FXML
    private TextField funcaoTexto;
    @FXML
    private Button cadastrarButton;
    @FXML
    private Button atualizarButton;
    @FXML
    private Button deletarButton;
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> nomeColuna;
    @FXML
    private TableColumn<Pessoa, Integer> idadeColuna;
    @FXML
    private TableColumn<Pessoa, String> enderecoColuna;
    @FXML
    private TableColumn<Pessoa, String> tipoColuna;
    
    private ObservableList<Pessoa> pessoas;
    
    @FXML
    private void cadastrarButton(ActionEvent event){
        if(alunoButton.isSelected()){
            Aluno aluno = new Aluno();
            preencherObjeto(aluno);
            aluno.setCurso(cursoTexto.getText());
            aluno.setSemestre(semestreTexto.getText());
            aluno.setTipo("aluno");
            pessoas.add(aluno);
        }
        else if(professorButton.isSelected()){
            Professor professor = new Professor();
            preencherObjeto(professor);
            professor.setSalario(Double.valueOf(salarioProfessorTexto.getText()));
            professor.setDisciplina(disciplinaTexto.getText());
            professor.setTipo("professor");
            pessoas.add(professor);
        }else{
            FuncaoAdministrativa adm = new FuncaoAdministrativa();
            preencherObjeto(adm);
            adm.setFuncao(funcaoTexto.getText());
            adm.setSetor(setorTexto.getText());
            adm.setTipo("adm");
            pessoas.add(adm);
        }
        limparTudo();
    }
    
    @FXML
    private void mostrarFormularioTipo(){
        limparFormulariosTipo();
        if(alunoButton.isSelected()){
            cadastroAluno.setVisible(true);
            cadastroProfessor.setVisible(false);
            CadastroAdm.setVisible(false);
        }
        else if(professorButton.isSelected()){
            cadastroAluno.setVisible(false);
            cadastroProfessor.setVisible(true);
            CadastroAdm.setVisible(false);
        }
        else{
            cadastroAluno.setVisible(false);
            cadastroProfessor.setVisible(false);
            CadastroAdm.setVisible(true);
        }
    }
    
    @FXML
    private void getCadastroOnTable(){
        if(tabela.getSelectionModel().getSelectedItem() != null)
        {
            limparTudo();
            cadastrarButton.setDisable(true);
            Pessoa pessoa = tabela.getSelectionModel().getSelectedItem();
            atualizarButton.setDisable(false);
            deletarButton.setDisable(false);
            
            nomeTexto.setText(pessoa.getNome());
            idadeTexto.setText(String.valueOf(pessoa.getIdade()));
            enderecoTexto.setText((pessoa.getEndereco()));
            if("aluno".equals(pessoa.getTipo())){
                Aluno aluno = (Aluno) pessoa;
                professorButton.setDisable(true);
                funcaoAdministrativaButton.setDisable(true);
                alunoButton.setSelected(true);
                cadastroAluno.setVisible(true);
                semestreTexto.setText(aluno.getSemestre());
                cursoTexto.setText(aluno.getCurso());
            } else if("professor".equals(pessoa.getTipo())){
                Professor professor = (Professor) pessoa;
                alunoButton.setDisable(true);
                funcaoAdministrativaButton.setDisable(true);
                professorButton.setSelected(true);
                cadastroProfessor.setVisible(true);
                salarioProfessorTexto.setText(String.valueOf(professor.getSalario()));
                disciplinaTexto.setText(professor.getDisciplina());
            }
            else{
                FuncaoAdministrativa adm = (FuncaoAdministrativa) pessoa;
                alunoButton.setDisable(true);
                professorButton.setDisable(true);
                funcaoAdministrativaButton.setSelected(true);
                CadastroAdm.setVisible(true);
                salarioAdmTexto.setText(String.valueOf(adm.getSalario()));
                setorTexto.setText(adm.getSetor());
                funcaoTexto.setText(adm.getFuncao());
            }
        }
    }
    
    @FXML
    private void atualizarButton(){
        Pessoa pessoa = tabela.getSelectionModel().getSelectedItem();
        if(alunoButton.isSelected()){
            Aluno aluno = (Aluno) pessoa;
            preencherObjeto(aluno);
            aluno.setCurso(cursoTexto.getText());
            aluno.setSemestre(semestreTexto.getText());
        }
        else if(professorButton.isSelected()){
            Professor professor = (Professor) pessoa;
            preencherObjeto(professor);
            professor.setSalario(Double.valueOf(salarioProfessorTexto.getText()));
            professor.setDisciplina(disciplinaTexto.getText());
        }else{
            FuncaoAdministrativa adm = (FuncaoAdministrativa) pessoa;
            preencherObjeto(adm);
            adm.setFuncao(funcaoTexto.getText());
            adm.setSetor(setorTexto.getText());
        }
        tabela.refresh();
        limparTudo();
    }
    
    @FXML
    private void deletarButton(){
        pessoas.remove(tabela.getSelectionModel().getSelectedItem());
        limparTudo();
    }
    
    @FXML
    private void limparTudo(){
        limparFormulariosTipo();
        nomeTexto.clear();
        idadeTexto.clear();
        enderecoTexto.clear();
        alunoButton.setSelected(false);
        alunoButton.setDisable(false);
        professorButton.setSelected(false);
        professorButton.setDisable(false);
        funcaoAdministrativaButton.setSelected(false);
        funcaoAdministrativaButton.setDisable(false);
        atualizarButton.setDisable(true);
        deletarButton.setDisable(true);
        cadastrarButton.setDisable(false);
        cadastroAluno.setVisible(false);
        cadastroProfessor.setVisible(false);
        CadastroAdm.setVisible(false);
    }
    
    private void limparFormulariosTipo(){
        salarioAdmTexto.clear();
        salarioProfessorTexto.clear();
        semestreTexto.clear();
        cursoTexto.clear();
        disciplinaTexto.clear();
        setorTexto.clear();
        funcaoTexto.clear();
    }
    
    private void preencherObjeto(Pessoa pessoa){
        pessoa.setNome(nomeTexto.getText());
        pessoa.setIdade(Integer.valueOf(idadeTexto.getText()));
        pessoa.setEndereco(enderecoTexto.getText());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizarButton.setDisable(true);
        deletarButton.setDisable(true);
        pessoas = tabela.getItems();
        
        nomeColuna.setCellValueFactory(new PropertyValueFactory<>("nome"));
        idadeColuna.setCellValueFactory(new PropertyValueFactory<>("idade"));  
        enderecoColuna.setCellValueFactory(new PropertyValueFactory<>("endereco"));     
        tipoColuna.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        
             
        
        for(int i=0; i<3; i++){
            Aluno aluno = new Aluno();
            aluno.setNome("Carla" + i);
            aluno.setIdade(1+i);
            aluno.setEndereco("Ruatal-" + i);
            aluno.setSemestre("terceiro" + i);
            aluno.setCurso("ds-1" + i);
            aluno.setTipo("aluno");
            pessoas.add(aluno);
        }
        for(int i=0; i<3; i++){
            Professor professor = new Professor();
            professor.setNome("João" + 1);
            professor.setIdade(30 + i);
            professor.setEndereco("Aquela Rua" + i);
            professor.setSalario(1000 + i);
            professor.setDisciplina("Informática-" + i);
            professor.setTipo("professor");
            pessoas.add(professor);
        }
        for(int i=0; i<3; i++){
            FuncaoAdministrativa adm = new FuncaoAdministrativa();
            adm.setNome("Alemoa" + i);
            adm.setIdade(21 + i);
            adm.setEndereco("Naquela Rua" + i);
            adm.setSalario(1500 + i);
            adm.setFuncao("Tesoureira-" + i);
            adm.setTipo("adm");
            pessoas.add(adm);
        }
        
    }
}
